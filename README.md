# Jenkins Training

# Reprequisites
- Docker
- Jenkins
- Groovy

# Jenkins Deployment
- Virtual Machine
- Jenkins in Docker (DinD)
  - https://www.jenkins.io/doc/book/installing/docker/
- Jenkins in Docker (DooD)
  - https://blog.container-solutions.com/running-docker-in-jenkins-in-docker

# TODO
- Connect to `Jenkins in Docker`'s Workspaces in VSCode