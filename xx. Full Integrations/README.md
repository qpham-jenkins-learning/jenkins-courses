# Provisioning Jenkins Server
- Jenkins in Docker
- Jenkins Configuration as Code
  - https://plugins.jenkins.io/configuration-as-code/
  - https://github.com/Ouranosinc/jenkins-config
  - https://github.com/emmetog/ansible-jenkins
  - https://github.com/Praqma/praqma-jenkins-casc

# Global Tool Configuration
- No need because every job is running in Docker.

# Plugins
-

# Testing & Report
- Send Notification
  - Email
  - Slack

# Jenkins Distributed Build Architecture
- Master - Slaves

# Job DSL scripts
- Maven Project
  - Preparation
    - Parameters
    - Enviroment - Docker container
    - Environemnt Variables
    - Credentials
    - Options (timeout, retry, build discarding)
  - SCM - Github Webhook
  - Track Git Build Data
  - Test
    - Unit Test
    - Integration Test
  - Archive the test results
    - Line coverage - JaCoCo
    - Code coverage and metrics - Cobertura
  - Build
  - Package
  - Publish
  - Deploy to testing environment
  - Smock test
  - System Test
    - End to end testing - Selenium Grid
  - Archive the test results
    - Test Result
    - Test Summary Report - Selenium
  - Wait for approval to deploy on upper environment
  - Deploy on Production environment
  - Smock test

* Notification as needed for every failed/succeed step.
* Embeddable Build Status Icon
* Job DSL API Reference
* Changes

# Jenkins Authentication
- Active Directory
- Matrix Authorization Strategy

# Developing Jenkins Pipeline Script
- https://www.jenkins.io/blog/2016/04/14/replay-with-pipeline/
- VSCode Plugin

# Jenkins Websites
- https://plugins.jenkins.io/
- https://jenkinsci.github.io/job-dsl-plugin/

# SeedJob

# Others
- Themes
- Jenkins Blue Ocean
- Build Pipeline & Dependency Graph
- Restart Safely
- Lockable Resources
- Check File Fingerprint
- Job Import Plugins
- Disk Usage
- Monitoring of Jenkins master/slaves
- Splunk App for Jenkins
- Visual Pipeline Editor
- Rebuilder
- Project Relationship - Build Graph

# References
- https://www.bogotobogo.com/DevOps/Jenkins/Jenkins_Install.php
- 