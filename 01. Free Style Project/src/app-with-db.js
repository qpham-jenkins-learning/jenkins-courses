const express = require('express');
const app = express();
const mysql = require("mysql");

const connection = mysql.createConnection({
  host: process.env.MYSQL_HOST || 'localhost',
  user: process.env.MYSQL_USER || 'foo',
  password: process.env.MYSQL_PASSWORD || 'bar',
  database: process.env.MYSQL_DATABASE || 'test_db'
});

connection.connect(function(err){
  if(err){
    console.log('Oops error occurred when trying to connect to database:', err);
    return;
  }
  console.log('Connection to db established');
  connection.query(`CREATE TABLE IF NOT EXISTS users (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, username VARCHAR(32))`, err => {
    if(err) throw err;
    console.log('Table created!');
  });
});

app.get('/', (_, res) => {
  connection.query(`INSERT INTO users (username) values (?)`, 'newuser', (err, dbRes) => {
    if(err) throw err;
    res.send('You are visitor number ' + dbRes.insertId);
  });
});

const server = app.listen(3000, () => {
  console.log(`Server is listening on port ${server.address().port}`);
});