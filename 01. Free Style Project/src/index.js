const express = require('express');
const app = express();

app.get('/', (_, res) => {
  res.send('Hello World!');
});

const server = app.listen(3000, () => {
  console.log(`Server is listening on port ${server.address().port}`);
});