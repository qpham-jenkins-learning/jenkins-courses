# Free Style Project

# Credentials
- Docker Hub

# Tools
- NodeJS tools

# Plugins
- CloudBees Docker

# Job configuration
## Source Code Management
- GIT

## Build
- Execute shell
```bash
npm install
```

- Docker Build and Publish
  - Repository Name
  - Registry credentials

# For setup mysql in Docker
- start mysql
```bash
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=root@1234 -d mysql
```

- connect to mysql
```bash
docker exec -it __container_id__ bash
#
container> mysql -u root -p
# Password: root@1234
#
mysql>
```

- create new user
```sql
CREATE USER 'foo'@'%' IDENTIFIED BY 'bar';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```